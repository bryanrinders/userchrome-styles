#+TITLE: UserChrome Styles For Firefox
#+SUBTITLE: README
#+AUTHOR: Bryan Rinders
#+DATE: <2023-09-25>
#+OPTIONS: ^:{} toc:nil num:nil

* userChrome
This project provides 2 userChrome styles for Firefox to make the UI
more compact.
- =src/userChrome-hover.css=: move the address and tabs bar to the
  bottom and only show the tabs bar when hovering over the address
  bar.
- =src/userChrome-compact.css=: align the address and tabs bar.


Additionally the =user.js= will enable a more compact UI and enable
customization by userChrome.css.

* Supported Version(s)
- Firefox 115.0

* Installation
Let =PROFILE= be the profile you wish to customize with directory
=~/.mozilla/<profile>=.

1. Clone this repo.
2. Place =user.js= inside =PROFILE=
3. Place one of the =chrome/userChrome-*.css= files of this repo
   inside =PROFILE/chrome=.
4. Restart ~firefox~.

#+begin_src sh
  PROFILE=$HOME/.mozilla/firefox/abcdefgh.default

  git clone https://gitlab.com/bryanrinders/userchrome-styles.git
  cd userchrome-styles

  cp user.js "$PROFILE"

  mkdir "$PROFILE"/chrome
  cp chrome/userChrome-hover.css "$PROFILE"/chrome/userChrome.css
#+end_src

* Sources
- =src/userChrome-compact.css= is a slightly modified version of [[https://github.com/dave-luna/Compact-Firefox-UI][this]]
  repo.
- =src/userChrome-hover.css= was gathered partially from
  [[https://github.com/Arty2/userstyles/blob/b42d13ce50c5ffec087cff9dac6c7cf56e999473/tabs_to_bottom.userchrome.css][tabs_to_bottom]] and some unknown source.
